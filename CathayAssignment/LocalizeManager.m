//
//  LocalizeManager.m
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import "LocalizeManager.h"

NSString *_Nonnull const kStringUpdateNotification = @"kStringUpdateNotification";

@interface LocalizeManager(){
    NSMutableDictionary *_StringPatchDictionary;
}

@property (nonnull,nonatomic,strong) NSBundle *bundle;
@property (nonnull,nonatomic,strong) NSString *languageKey;

@end

@implementation LocalizeManager

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

/**
 取的多語系字串
 
 @param key String id
 
 @return Localized string
 */
+(NSString *)l10n:(NSString *)key{
    return [[LocalizeManager sharedInstance] getLocalizationString:key];
}

-(instancetype)init{
    self = [super init];
    if (self) {
        _StringPatchDictionary = [NSMutableDictionary new];
        self.languageKey = [self getDeviceLanguage];
        [self setBundleWithLanguageKey:self.languageKey];
        
        // PATCH
        [self apiGetLocalizationString:^(NSDictionary *patchDictionary) {
            if ([_StringPatchDictionary isEqual:patchDictionary] == false) {
                
                [_StringPatchDictionary setDictionary:patchDictionary];
                
                // 發出Event 通知 BaseViewController
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kStringUpdateNotification object:nil];
                });
            }
        }];
    }
    return self;
}

#pragma mark - Private method

-(void) setLanguageKey:(NSString *)languageKey{
    // 實作語系切換
    _languageKey = languageKey;
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:languageKey ofType:@"lproj"];
    
    // 例外處理: 還原為default bundle
    if (bundlePath == nil) {
        _languageKey = [self getDeviceLanguage];
    }
    [self setBundleWithLanguageKey:self.languageKey];
}

- (void) setBundleWithLanguageKey:(NSString *)languageKey{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:languageKey ofType:@"lproj"];
    self.bundle = [[NSBundle alloc] initWithPath:bundlePath];
}

/**
 取得系統preferred的語系，可在此作保留In-app的語系切換的彈性
 
 @return Preferred language
 */
-(NSString *) getDeviceLanguage{
    NSString *language = [NSLocale preferredLanguages].firstObject;
    return language;
}

-(NSString *) getLocalizationString:(NSString *)key{
    
    // 實作方案二(PATCH)
    NSString *localizedString = [_StringPatchDictionary objectForKey:key];
    if (localizedString == nil) {
        localizedString = [self.bundle localizedStringForKey:key value:key table:nil];
    }
    return localizedString;
}


/**
 與Server溝通取的更新後的字串

 @param completeBlock 完成後回調
 */
- (void) apiGetLocalizationString:(void (^)(NSDictionary *patchDictionary))completeBlock{
    
    // 模擬HTTP行為與資料
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.rick.cathay.assignment", NULL);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), backgroundQueue, ^{
        completeBlock(@{@"cathay_assignment_first_string":@"更新後字串",
                      @"cathay_assignment_add_new_string":@"新增字串"});
    });
}

#pragma mark - Public Method

-(void) changeLanguage:(NSString *)languageKey{
    [self setLanguageKey:languageKey];
}
@end
