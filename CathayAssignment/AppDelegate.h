//
//  AppDelegate.h
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

