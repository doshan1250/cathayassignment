//
//  ViewController.m
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import "ViewController.h"
#import "LocalizeManager.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;

@end

@implementation ViewController

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - Override
-(void)setString{
    
    self.firstLabel.text = [LocalizeManager l10n:@"cathay_assignment_first_string"];
    self.secondLabel.text = [LocalizeManager l10n:@"cathay_assignment_add_new_string"];
}

#pragma mark - System
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
