//
//  CathayBaseViewController.m
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import "CathayBaseViewController.h"
#import "LocalizeManager.h"

@interface CathayBaseViewController ()

@end

@implementation CathayBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setString];
    [self addObserver];
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setString)
                                                 name:kStringUpdateNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setString{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
