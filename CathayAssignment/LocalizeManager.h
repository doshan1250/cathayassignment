//
//  LocalizeManager.h
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *_Nonnull const kStringUpdateNotification;

@interface LocalizeManager : NSObject

/**
 取的多語系字串

 @param key String id

 @return Localized string
 */
+(NSString *_Nonnull)l10n:(NSString *_Nonnull)key;

/**
 切換語系，置換為目標語系的bundle
 
 @param languageKey target language
 */
-(void) changeLanguage:(NSString *_Nonnull)languageKey;

@end
