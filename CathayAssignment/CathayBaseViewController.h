//
//  CathayBaseViewController.h
//  CathayAssignment
//
//  Created by Rick Chuang on 2016/9/27.
//  Copyright © 2016年 Rick Chuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CathayBaseViewController : UIViewController

/**
 Subclass須將更新字串的代碼寫在setString內，
 當語系或字串變化時會觸發此method。
 */
- (void)setString;

@end
